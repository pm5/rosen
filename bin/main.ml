open Rosen
open Napkin
open Stdio

let () =
  let open Result in
  unwrap_unsafe
    (let* config = In_channel.with_file "bridge.toml" ~f:Config.from_channel in

     Map.for_each config.slack ~f:(fun protocol ->
         printf "protocol: %s\n" protocol.name);

     Map.for_each config.mattermost ~f:(fun protocol ->
         printf "protocol: %s\n" protocol.name);

     Map.for_each config.discord ~f:(fun protocol ->
         printf "protocol: %s\n" protocol.name);

     let bridge_map = BridgeMap.full_map in
     let router = Gateway.Router.create ~config ~bridge_map in
     let gateways =
       config.gateway
       |> List.map ~f:(fun gateway ->
              Gateway.Gateway.create ~gateway ~config ~router)
     in

     let* runners =
       gateways
       |> List.map ~f:(fun gw -> Gateway.Gateway.start gw)
       |> Result.values
     in

     (* checks *)
     gateways
     |> List.for_each ~f:(fun gw -> printf "%s\n" gw.Gateway.Gateway.name);

     ok ())
