open Napkin

exception StartupError
type bridge = Bslack of Bslack.t | Btelegram of Btelegram.t

module rec Gateway : sig
  type t = {
    name : string;
    gateway : Config.Gateway.t;
    config : Config.t;
    router : Router.t;
    bridges : bridge Map.String.t;
    thread : Thread.t option;
    message : Bridge.Message.t Event.channel;
  }

  val create :
    gateway:Config.Gateway.t -> config:Config.t -> router:Router.t -> t

  val start : t -> (unit, exn) result
end = struct
  type t = {
    name : string;
    gateway : Config.Gateway.t;
    config : Config.t;
    router : Router.t;
    bridges : bridge Map.String.t;
    thread : Thread.t option;
    message : Bridge.Message.t Event.channel;
  }

  let create ~gateway ~config ~router =
    let message = Event.new_channel () and bridges = Map.String.empty in
    {
      name = gateway.Config.Gateway.name;
      gateway;
      config;
      router;
      bridges;
      message;
      thread = None;
    }

  let start gateway = Ok ()
end

and Router : sig
  type t

  val start : t -> (unit, string) result
  val create : config:Config.t -> bridge_map:BridgeMap.t -> t
  (*val handle_receive: *)
end = struct
  type t = {
    config : Config.t;
    bridge_map : BridgeMap.t;
    mutable gateways : Gateway.t Map.String.t;
  }

  let create ~config ~bridge_map =
    { config; bridge_map; gateways = Map.String.empty }

  let add_gateways router =
    let { config } = router in
    let gateways =
      config.gateway
      |> List.map ~f:(fun gateway ->
             ( gateway.Config.Gateway.name,
               Gateway.create ~gateway ~config ~router ))
      |> Map.String.from_list
    in
    router.gateways <- gateways

  let start router = Ok ()
end
