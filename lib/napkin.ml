include Tablecloth

module Result = struct
  open Tablecloth
  include Result

  let ( <$> ) f t = Result.map t ~f
  let ( <*> ) f t = Result.and_then t ~f
  let ( let* ) t f = Result.and_then t ~f
  let ( let+ ) t f = Result.map t ~f
  let ( and+ ) t1 t2 = Result.both t1 t2
  let unwrap_unsafe = Base.Result.ok_exn
end

module Option = struct
  include Tablecloth.Option

  let ( let* ) t f = Option.and_then t ~f

  let unwrap_unsafe
      ?(error = Invalid_argument "Option.unwrap_unsafe called with None") t =
    match t with None -> raise error | Some t -> t

  let ( |! ) t error = unwrap_unsafe ~error t

  let ( == ) a b =
    match (a, b) with
    | None, None -> true
    | Some a', Some b' -> a' == b'
    | _ -> false
end
