type t
type value

val from_channel : in_channel -> t
val from_string : string -> t
val get_bool : t -> string -> bool option
val get_string : t -> string -> string option
val get_table : t -> string -> t option
val get_tables : t -> string -> t list option
val get_keys : t -> string list
