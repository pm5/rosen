open Napkin

type error = InvalidToken
type t = { token : string; channel : string }

module Config = struct
  type t = { token : string; channel : string }
end

type config = Config.t

module Connection = struct
  type t = { token : string; channel : string; session : string }
end

type connection = Connection.t

let from_config config =
  { token = config.token; channel = config.channel }

let connect (b : t) : (Connection.t, error) result =
  Ok { Connection.token = b.token; channel = b.channel; session = "newnewnew" }

let join_channel (c : Connection.t) channel = Ok ()
let send (c : Connection.t) (message : Bridge.Message.t) : (unit, error) result = Ok ()

let send_webhook (c : Connection.t) (message : Bridge.Message.t) : (unit, error) result =
  Ok ()

let disconnect (c : Connection.t) : (unit, error) result = Ok ()
