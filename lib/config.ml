open Napkin

module SlackProtocol = struct
  type t = {
    name : string;
    token : string;
    remote_nick_format : string;
    preserve_threading : bool;
  }
end

module MattermostProtocol = struct
  type t = {
    name : string;
    server : string;
    team : string;
    token : string;
    remote_nick_format : string;
    prefix_messages_with_nick : bool;
  }
end

module DiscordProtocol = struct
  type t = {
    name : string;
    token : string;
    server : string;
    remote_nick_format : string;
    preserve_threading : bool;
    auto_webhooks : bool;
  }
end

module Bridge = struct
  type t = { account : string; channel : string }
end

module Gateway = struct
  type t = { name : string; enable : bool; inout : Bridge.t list }
end

type t = {
  slack : SlackProtocol.t Map.String.t;
  mattermost : MattermostProtocol.t Map.String.t;
  discord : DiscordProtocol.t Map.String.t;
  gateway : Gateway.t list;
}

let from_toml_unsafe toml_data =
  let get_protocol_map toml_data key ~f =
    Option.(
      let* table = Eztoml.get_table toml_data key in
      Eztoml.get_keys table
      |> List.filter_map ~f:(fun name ->
             let* protocol = Eztoml.get_table table name in
             Some (key ^ "." ^ name, f name protocol))
      |> Map.String.from_list |> some)
  in
  let slack =
    Option.(
      get_protocol_map toml_data "slack" ~f:(fun name protocol ->
          Eztoml.
            {
              SlackProtocol.name = "slack." ^ name;
              token =
                get_string protocol "Token"
                |! Invalid_argument ("No token for slack." ^ name);
              remote_nick_format =
                get_string protocol "RemoteNickFormat" |? "{NICK}@{BRIDGE}";
              preserve_threading = get_bool protocol "PreserveThreading" |? true;
            })
      |> unwrap ~default:Map.String.empty)
  and mattermost =
    Option.(
      get_protocol_map toml_data "mattermost" ~f:(fun name protocol ->
          Eztoml.
            {
              MattermostProtocol.name = "mattermost." ^ name;
              server =
                get_string protocol "Server"
                |! Invalid_argument ("No server for mattermost." ^ name);
              team =
                get_string protocol "Team"
                |! Invalid_argument ("No team for mattermost." ^ name);
              token =
                get_string protocol "Token"
                |! Invalid_argument ("No token for mattermost." ^ name);
              remote_nick_format =
                get_string protocol "RemoteNickFormat" |? "{NICK}@{BRIDGE}";
              prefix_messages_with_nick =
                get_bool protocol "PrefixMessagesWithNick" |? true;
            })
      |> unwrap ~default:Map.String.empty)
  and discord =
    Option.(
      get_protocol_map toml_data "discord" ~f:(fun name protocol ->
          Eztoml.
            {
              DiscordProtocol.name = "discord." ^ name;
              token =
                get_string protocol "Token"
                |! Invalid_argument ("No token for discord." ^ name);
              server =
                get_string protocol "Server"
                |! Invalid_argument ("No server for discord." ^ name);
              remote_nick_format =
                get_string protocol "RemoteNickFormat" |? "{NICK}@{BRIDGE}";
              preserve_threading = get_bool protocol "PreserveThreading" |? true;
              auto_webhooks = get_bool protocol "AutoWebhooks" |? true;
            })
      |> unwrap ~default:Map.String.empty)
  in

  let get_bridge toml_data key =
    Option.(
      Eztoml.(
        get_tables toml_data key
        >>| List.map ~f:(fun table ->
                {
                  Bridge.account =
                    get_string table "account" |! Invalid_argument "No account";
                  channel =
                    get_string table "channel" |! Invalid_argument "No channel";
                })))
  in

  let gateway =
    Option.(
      Eztoml.(
        get_tables toml_data "gateway"
        |? []
        |> List.map ~f:(fun table ->
               {
                 Gateway.name =
                   get_string table "name" |! Invalid_argument "No name";
                 enable = get_bool table "enable" |? true;
                 inout = get_bridge table "inout" |? [];
               })))
  in

  { slack; mattermost; discord; gateway }

let from_toml ?(silent = false) toml_data =
  Result.(
    try ok (from_toml_unsafe toml_data)
    with Invalid_argument message ->
      if not silent then Stdio.printf "[warning] %s" message;
      error (Invalid_argument message))

let from_channel ic = Eztoml.from_channel ic |> from_toml
let from_string s = Eztoml.from_string s |> from_toml

let empty =
  {
    slack = Map.String.empty;
    mattermost = Map.String.empty;
    discord = Map.String.empty;
    gateway = [];
  }
