module Message = struct
  type t = { text : string }
end

module ChannelInfo = struct
  type t = { name : string }
end

module ConversationInfo = struct
  type t = { conversation_id: string }
end

module type Bridger = sig
  type t
  type config
  type connection
  type error

  val connect : t -> (connection, error) result
  val join_channel : connection -> ChannelInfo.t -> (unit, error) result
  val send : connection -> Message.t -> (unit, error) result
  val disconnect : connection -> (unit, error) result
end

module type Slack = sig
  include Bridger

  val send_webhook : connection -> Message.t -> (unit, error) result
end

module type Telegram = sig
  include Bridger

  val create_attach : connection -> Message.t -> (unit, error) result
end

type factory = Slack of (module Slack) | Telegram of (module Telegram)
