open Napkin

type t = Toml.Types.table
type value = Toml.Types.value

let from_string toml_string = Toml.Parser.(from_string toml_string |> unsafe)
let from_channel ic = Stdio.In_channel.input_all ic |> from_string

let get_bool toml_data k =
  Toml.Lenses.(get toml_data (key k |-- bool))

let get_string toml_data k =
  Toml.Lenses.(get toml_data (key k |-- string))

let get_table toml_data k =
  Toml.Lenses.(get toml_data (key k |-- table))

let get_tables toml_data k =
  Toml.Lenses.(get toml_data (key k |-- array |-- tables))

let get_keys toml_data =
  Toml.Types.Table.(
    to_seq toml_data |> Stdlib.List.of_seq
    |> List.map ~f:(fun (k, _) -> Key.to_string k))
