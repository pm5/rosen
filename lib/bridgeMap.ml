open Napkin

type t = Bridge.factory Map.String.t

let full_map : t =
  Map.String.from_list
    Bridge.
      [
        ("slack", Slack (module Bslack));
        ("telegram", Telegram (module Btelegram));
      ]
